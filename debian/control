Source: mricron
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Michael Hanke <mih@debian.org>,
           Yaroslav Halchenko <debian@onerussian.com>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               fp-units-misc,
               imagemagick,
               lcl-qt5,
               lcl-units,
               lcl-utils,
               libx11-dev,
               lazarus-src
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/mricron
Vcs-Git: https://salsa.debian.org/med-team/mricron.git
Homepage: https://www.nitrc.org/projects/mricron
Rules-Requires-Root: no

Package: mricron
Architecture: any-amd64 any-i386 ppc64el arm64 armel armhf ia64
Depends: mricron-data,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: pigz,
            dcm2niix
Suggests: mricron-doc
Description: magnetic resonance image conversion, viewing and analysis
 This is a GUI-based visualization and analysis tool for (functional) magnetic
 resonance imaging. MRIcron can be used to create 2D or 3D renderings of
 statistical overlay maps on brain anatomy images. Moreover, it aids drawing
 anatomical regions-of-interest (ROI), or lesion mapping, as well as basic
 analysis of functional timeseries (e.g. creating plots of peristimulus
 signal-change).
 .
 In addition to 'mricron', this package also provides 'dcm2nii' that supports
 converting DICOM and PAR/REC images into the NIfTI format, and 'npm' for
 non-parametric data analysis.

Package: mricron-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: data files for MRIcron
 This is a GUI-based visualization and analysis tool for (functional) magnetic
 resonance imaging. MRIcron can be used to create 2D or 3D renderings of
 statistical overlay maps on brain anatomy images. Moreover, it aids drawing
 anatomical regions-of-interest (ROI), or lesion mapping, as well as basic
 analysis of functional timeseries (e.g. creating plots of peristimulus
 signal-change).
 .
 This package provides data files for MRIcron, such as brain atlases, anatomy,
 and color schemes.

Package: mricron-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Description: data files for MRIcron
 This is a GUI-based visualization and analysis tool for (functional) magnetic
 resonance imaging. MRIcron can be used to create 2D or 3D renderings of
 statistical overlay maps on brain anatomy images. Moreover, it aids drawing
 anatomical regions-of-interest (ROI), or lesion mapping, as well as basic
 analysis of functional timeseries (e.g. creating plots of peristimulus
 signal-change).
 .
 This package provides documentation for MRIcron in HTML format.
